
import java.awt.Color;
import java.awt.image.BufferedImage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kairon
 */
public class PPM extends Picture {

    private int[][][] pixels;

    PPM(String path) {
        this.mountImage(path);
    }

    private void setPixels(int height, int width) {
        this.pixels = new int[height][width][3];
    }

    // Fill every column of a pixel.
    private void setPixel(int row, int col, int color, int value) {
        this.pixels[row][col][color] = value;
    }

    // Get the array of pixel
    public int[] getPixel(int row, int col) {
        return this.pixels[row][col];
    }
    
    public void mountImage(String path) {
        System.out.println(BufferedImage.TYPE_INT_RGB);
        this.open(path);
        this.setPixels(this.getHeight(), this.getWidth());
        for (int row = 0; row < this.getHeight(); row++) {
            for (int col = 0; col < this.getWidth(); col++) {
                try {
                    int red = this.getFile().read();
                    int green = this.getFile().read();
                    int blue = this.getFile().read();

                    this.setPixel(row, col, 0, red);
                    this.setPixel(row, col, 1, green);
                    this.setPixel(row, col, 2, blue);
                    this.getPicture().setRGB(col, row, new Color(red, green, blue).getRGB());
                } catch (Throwable t) {
                    t.printStackTrace(System.err);
                }
            }
        }
    }
    
    @Override
    public void applyFilterRGB(int color) {
        for (int row = 0; row < this.getHeight(); row++) {
            for (int col = 0; col < this.getWidth(); col++) {
                int[] rgb = new int[3]; // create a new array with zeros
                rgb[color] = this.getPixel(row, col)[color]; // define the color of desired filter from
                this.getPicture().setRGB(col, row, new Color(rgb[0], rgb[1], rgb[2]).getRGB());
            }
        }
    }
    
    @Override
    public void applyFilterNegative() {
        for (int row = 0; row < this.getHeight(); row++) {
            for (int col = 0; col < this.getWidth(); col++) {
                int[] rgb = new int[3];
                System.arraycopy(this.getPixel(row, col), 0, rgb, 0, rgb.length);
                this.getPicture().setRGB(col, row, new Color(this.getMaxGrey()-rgb[0], this.getMaxGrey()-rgb[1], this.getMaxGrey()-rgb[2]).getRGB());
            }
        }
    }
}
