
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kairon
 */
public class PGM extends Picture {

    private int[][] pixels;
    private byte[] bytes;
    String message;

    PGM(String path) {
        this.setMessage("");
        this.mountImage(path);
    }

    private void setPixels(int height, int width) {
        this.pixels = new int[height][width];
    }

    private void setPixel(int row, int col, int color) {
        this.pixels[row][col] = color;
    }

    public int getPixel(int row, int col) {
        return this.pixels[row][col];
    }

    private void setBytes() {
        this.bytes = new byte[this.getSize()];
    }

    private void setByte(int pos, byte bb) {
        this.bytes[pos] = bb;
    }

    public byte getByte(int pos) {
        return this.bytes[pos];
    }

    private void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void mountImage(String path) {
        this.open(path);
        this.setPixels(this.getHeight(), this.getWidth());
        this.setBytes();

        for (int row = 0; row < this.getHeight(); row++) {
            for (int col = 0; col < this.getWidth(); col++) {
                try {
                    int color = this.getFile().read();
                    byte bb = (byte) color;
                    this.setByte(this.getWidth() * row + col, bb);

                    if (color < 0 || color > this.getMaxGrey()) {
                        color = 0;
                    }
                    this.setPixel(row, col, color);
                    this.getPicture().setRGB(col, row, new Color(color, color, color).getRGB());
                } catch (Throwable t) {
                    t.printStackTrace(System.err);
                }
            }
        }
        this.extractMessage();
    }

    public void extractMessage() {
        char character = 0;
        int count = 1;
        // get start position
        int start = Integer.parseInt(this.getComment().replaceAll("[\\D]", ""));

        /*
        int row = start / this.getHeight(); // calculate row by the start position
        int col = (start % this.getHeight()) - 1; // calculate col by the start position
        System.out.println(row);
        System.out.println(col);
         */
        for (int pos = start; pos < this.getSize(); pos++) {
            character <<= 1;
            character |= this.getByte(pos) & 0x01;
            if (count == 8) {             // Is it the least significant bit ? 
                if (character == '#') {   // Is it the end of the message ?
                    break;
                }
                this.setMessage(this.getMessage() + character);
                count = 0;                // RESET COUNT
                character = 0;            // RESET CHAR
            }
            count++;
        }
    }

    @Override
    public void saveMessage(String name) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(name+".txt"), "utf-8"))) {
            writer.write(this.getMessage());
            writer.close();
        } catch (Throwable e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void applyFilterNegative() {
        for (int row = 0; row < this.getHeight(); row++) {
            for (int col = 0; col < this.getWidth(); col++) {
                int color = this.getMaxGrey() - this.getPixel(row, col);
                this.getPicture().setRGB(col, row, new Color(color, color, color).getRGB());
            }
        }
    }

    /**
     *
     * @param option : the desired filter
     */
    @Override
    public void applyFilter(int option) {

        double filter[][][] = new double[3][3][3];

        // filter matrices
        double sharpen[][] = {{0, -1, 0}, {-1, -5, -1}, {0, -1, 0}};
        double smooth[][] = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        double blur[][] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

        filter[0] = sharpen;
        filter[1] = smooth;
        filter[2] = blur;

        // filter factors. this will multiply the sum "value"
        double factor[] = new double[]{0.1, 0.05, 0.06};

        // looping inside the edges
        for (int row = 1; row < this.getHeight() - 1; row++) {
            for (int col = 1; col < this.getWidth() - 1; col++) {

                double value = 0.0;
                // multiply matrices for a given position
                for (int row_filter = -1; row_filter < 2; row_filter++) {
                    for (int col_filter = -1; col_filter < 2; col_filter++) {
                        value += filter[option][row_filter + 1][col_filter + 1] * this.pixels[row + row_filter][col + col_filter];
                    }
                }

                // set the new pixel value
                int pixel = (int) (this.getPixel(row, col) - factor[option] * value);
                pixel = pixel < 0 ? 0 : pixel;
                pixel = pixel > 255 ? 255 : pixel;
                //System.out.println(pixel);
                //this.setPicturePixel(row, col, new Color(pixel, pixel, pixel));
                this.getPicture().setRGB(col, row, new Color(pixel, pixel, pixel).getRGB());
            }
        }
    }
}
